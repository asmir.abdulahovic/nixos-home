{ pkgs, config, lib, ... }:
with lib;
let
  sensitive = import ./sensitive.nix { inherit config; };
  vimrc = import ./vimrc.nix { inherit pkgs vimUtils fetchFromGitHub; };
in
{
  imports = [ ./zsh.nix ./i3status-rust.nix ./sway.nix ./i3.nix ];

  home.stateVersion = "22.11";
  home.username = "akill";
  home.homeDirectory = "/home/akill";

  xdg.enable = true;
  xdg.mimeApps = {
    enable = true;
    defaultApplications = {
      "application/pdf" = [ "sioyek.desktop" ];
    };
  };

  home.packages = with pkgs; [
    appimage-run
    arp-scan
    blackmagic
    btop
    cached-nix-shell
    ccls
    compsize
    cura
    deluge
    ffmpeg-full
    firefox
    gcc
    gdb
    glab
    glaxnimate
    gnumake
    grim
    hyperfine
    imagemagick
    imv
    kdenlive
    kicad
    libnotify
    libreoffice
    mediainfo
    ncdu
    ngspice
    nodePackages.peerflix
    openocd
    pandoc
    pass
    patchelf
    pavucontrol
    pirate-get
    powertop
    pulsemixer
    python39Packages.west
    river
    rtorrent
    screen
    seer
    sioyek
    slurp
    texlive.combined.scheme-full
    thunderbird
    upx
    waybar
    wdisplays
    whatsapp-for-linux
    wireshark
    wl-clipboard
    wlr-randr
    wofi
    x2goclient
    yt-dlp
    zathura
    zig
    zls
    rustc
    cargo
  ];

  fonts.fontconfig.enable = true;

  home.sessionVariables = {
    BROWSER = "qutebrowser";
    EDITOR = "nvim";
    _JAVA_AWT_WM_NONREPARENTING = "1";
    MOZ_ENABLE_WAYLAND = "1";
    #QT_QPA_PLATFORM = "wayland";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    #SDL_VIDEODRIVER = "wayland";
    SUDO_EDITOR = "nvim";
    WLR_RENDERER = "vulkan";
  };

  wayland.windowManager.sway = { enable = true; };

  programs = {
    home-manager.enable = true;
    mako.enable = true;

    librewolf = {
      enable = true;
      package = pkgs.librewolf-wayland;
      settings = {
        "webgl.disable" = true;
        "middlemouse.paste" = false;
      };
    };

    mpv = {
      enable = true;
      package = pkgs.mpv;
      config = {
        slang = "eng,en";
        alang = "eng,en";
        hwdec = "auto";
        vo = "gpu-next";
        ao = "pipewire";
        script-opts-set = "sponsorblock-local_database=no,sponsorblock-skip_categories=[sponsor,intro,selfpromo]";
        ytdl-format = "bestvideo[height<=?1080]+bestaudio/best";
      };

      bindings = {
        WHEEL_UP = "ignore";
        WHEEL_DOWN = "ignore";
        WHEEL_LEFT = "ignore";
        WHEEL_RIGHT = "ignore";
      };
    };

    himalaya = {
      enable = true;
      settings = {
        display-name = "Asmir A";
        downloads-dir = "~/Downloads";
        signature = ''
          - -
          Regards, Asmir A
        '';

        gmail = {
          default = true;
          backend = "imap";
          email = "asmir.abdulahovic@gmail.com";

          imap-host = "imap.gmail.com";
          imap-port = 993;
          imap-login = "asmir.abdulahovic@gmail.com";
          imap-passwd-cmd = "pass show asmir.abdulahovic@gmail.com";

          sender = "smtp";
          smtp-host = "smtp.gmail.com";
          smtp-port = 465;
          smtp-login = "asmir.abdulahovic@gmail.com";
          smtp-passwd-cmd = "pass show asmir.abdulahovic@gmail.com";
        };
      };
    };

    alacritty = {
      enable = true;

      settings = {
        font = {
          normal.family = "JetBrainsMono";
          italic.family = "JetBrainsMono";
          bold.family = "JetBrainsMono";
          bold_italic.family = "JetBrainsMono";
          size = 14.0;
        };

        selection = {
          text = "0xcfcfc2";
          background = "0x232629";

          normal = {
            black = "0x1c1b19";
            red = "0xef2f27";
            green = "0x519f50";
            yellow = "0xfbb829";
            blue = "0x2c78bf";
            magenta = "0xe02c6d";
            cyan = "0x0aaeb3";
            white = "0x918175";
          };

          bright = {
            black = "0x2D2C29";
            red = "0xf75341";
            green = "0x98bc37";
            yellow = "0xfed06e";
            blue = "0x68A8E4";
            magenta = "0xff5c8f";
            cyan = "0x53fde9";
            white = "0xfce8c3";
          };
        };
      };
    };

    rofi = {
      enable = true;
      theme = "gruvbox-dark";
    };

    foot = {
      enable = true;
      server.enable = true;
      settings = {
        main = {
          font = "JetBrainsMono:size=10";
          dpi-aware = "yes";
        };
        mouse = { hide-when-typing = "yes"; };
      };
    };

    qutebrowser = {
      enable = true;
      package = pkgs.qutebrowser;
      keyBindings = {
        normal = {
          "j" = "scroll-px 0 25";
          "k" = "scroll-px 0 -25";
          "u" = "undo --window";
          ";v" = "hint links userscript view_in_mpv";
        };
      };

      settings = {
        content.notifications.enabled = false;
        content.pdfjs = true;
        content.webgl = false;
        fonts.completion.category = "14pt monospace";
        fonts.completion.entry = "14pt monospace";
        fonts.contextmenu = "14pt monospace";
        fonts.debug_console = "14pt monospace";
        fonts.downloads = "14pt monospace";
        fonts.hints = "14pt monospace";
        fonts.keyhint = "14pt monospace";
        fonts.messages.info = "14pt monospace";
        fonts.prompts = "14pt monospace";
        fonts.statusbar = "14pt monospace";
        hints.chars = "1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik9ol0p";
        tabs.last_close = "close";
        tabs.position = "left";
        tabs.show = "multiple";
        tabs.tabs_are_windows = true;
        tabs.width = "12%";
        zoom.default = "125%";
      };
    };

    git = {
      enable = true;
      userName = "Asmir A";
      userEmail = "asmir.abdulahovic@gmail.com";
      extraConfig = {
        pull = { rebase = true; };
        credential = { helper = "store"; };
      };
    };

    neovim = {
      enable = true;
      vimAlias = true;
      vimdiffAlias = true;
      plugins = vimrc.plugins;
      extraConfig = "";
      extraPackages = vimrc.extraPackages;
    };

    i3status-rust.enable = true;
    z-lua.enable = true;
    zsh.enable = true;
  };

  xdg.configFile."nvim/init.lua" = {
    source = ./vimrc.lua;
    recursive = true;
  };

  services = {
    lorri.enable = false;
    gammastep = {
      enable = true;
      latitude = sensitive.latitude;
      longitude = sensitive.longitude;
      temperature.day = 5500;
      temperature.night = 2900;
    };

    gnome-keyring.enable = true;

    gpg-agent = {
      enable = true;
      defaultCacheTtl = 1800;
      enableSshSupport = true;
    };

    swayidle = {
      enable = true;
      events = [
        { event = "before-sleep"; command = "swaylock_bg_blur.sh"; }
        { event = "lock"; command = "swaylock_bg_blur.sh"; }
        { event = "after-resume"; command = "pkill -USR1 i3status-rs"; }
      ];
      timeouts = [
        { timeout = 15 * 60; command = "swaylock_bg_blur.sh"; }
      ];
    };

  };

  systemd.user = {
    services = {
      /* himalaya = {
        Unit.Description = "Himalaya new messages notifier";
        Service = {
        ExecStart = "himalaya notify";
        Restart = "always";
        RestartSec = 10;
        };
        Install = {
        WantedBy = [ "multi-user.target" ];
        };
        };
      */
    };
  };
}

