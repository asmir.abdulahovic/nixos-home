{ config, lib, pkgs, ... }:
let
  scratchpad_cmd = "floating enable, resize set 1502 845, move position center, move scratchpad, scratchpad show";
in
{
  xsession.windowManager.i3 = {
    enable = true;
    package = pkgs.i3;

    extraConfig = ''
      new_window pixel 3
      hide_edge_borders smart
    '';

    config = rec {
      modifier = "Mod1";


      keybindings = lib.mkOptionDefault {
        "${modifier}+Shift+Return" = "exec alacritty";
        "${modifier}+m" = "layout toggle splith tabbed";
        "${modifier}+t" = "split toggle";
        "${modifier}+s" = "layout toggle split";

        "${modifier}+1" = "workspace 1";
        "${modifier}+2" = "workspace 2";
        "${modifier}+3" = "workspace 3";
        "${modifier}+4" = "workspace 4";
        "${modifier}+5" = "workspace 5";
        "${modifier}+6" = "workspace 6";
        "${modifier}+7" = "workspace 7";
        "${modifier}+8" = "workspace 8";
        "${modifier}+9" = "workspace 9";
        "${modifier}+0" = "workspace 10";

        "${modifier}+Shift+1" = "move container to workspace 1";
        "${modifier}+Shift+2" = "move container to workspace 2";
        "${modifier}+Shift+3" = "move container to workspace 3";
        "${modifier}+Shift+4" = "move container to workspace 4";
        "${modifier}+Shift+5" = "move container to workspace 5";
        "${modifier}+Shift+6" = "move container to workspace 6";
        "${modifier}+Shift+7" = "move container to workspace 7";
        "${modifier}+Shift+8" = "move container to workspace 8";
        "${modifier}+Shift+9" = "move container to workspace 9";
        "${modifier}+Shift+0" = "move container to workspace 10";

        "${modifier}+h" = "focus left";
        "${modifier}+j" = "focus down";
        "${modifier}+k" = "focus up";
        "${modifier}+l" = "focus right";
        "${modifier}+slash" = "exec ${pkgs.rofi}/bin/rofi -show window";
        "${modifier}+Escape" = "workspace back_and_forth";
        "${modifier}+p" = "exec ${pkgs.dmenu}/bin/dmenu_run";

        "Mod4+l" = "exec i3-msg [instance=\"python3_scr\"] scratchpad show || exec alacritty --class python3_scr -e python3";
        "Mod4+j" = "exec i3-msg [class=\"ViberPC\"] scratchpad show || exec viber";
        "Mod4+m" = "exec i3-msg [class=\"Thunderbird\"] scratchpad show || exec thunderbird";
        "Mod4+y" = "exec i3-msg [instance=\"pulsemixer_scr\"] scratchpad show || exec alacritty --class pulsemixer_scr -e pulsemixer";

      };


      window = {
        /*border = 4;*/
        commands = [
          {
            command = scratchpad_cmd;
            criteria = { instance = "pulsemixer_scr|python3_scr"; };
          }
          {
            command = scratchpad_cmd;
            criteria = { class = "Thunderbird"; };
          }
          {
            command = scratchpad_cmd;
            criteria = { class = "ViberPC"; title = "Viber"; };
          }
          {
            command = "focus child, layout tabbed, focus";
            criteria = { class = "qutebrowser"; };
          }
        ];
      };

      bars = [
        {
          position = "top";
          fonts = {
            names = [ "DejaVu Sans Mono" "FontAwesome5Free" ];
            style = "Fixed Bold SemiCondensed";
            size = 7.0;
          };
          statusCommand = "i3status-rs /home/akill/.config/i3status-rust/config-top.toml";
          extraConfig = ''
            workspace_min_width 30
          '';
        }
      ];

    };
  };
}
