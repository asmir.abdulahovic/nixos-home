{ config, lib, pkgs, ... }:
{
  programs.z-lua = {
    enableAliases = true;
    enableZshIntegration = true;
  };

  programs.zsh = {
    autocd = true;
    enableCompletion = false;
    enableSyntaxHighlighting = true;
    defaultKeymap = "viins";

    shellAliases = {
      cfind = "cscope -C -R -L1";
      chmod = "chmod -v";
      chown = "chown -v";
      cp = "cp -v";
      rm = "rm -v";
      ip = "ip --color=auto";
    };

    history = {
      expireDuplicatesFirst = true;
      extended = true;
      save = 100000;
      size = 100000;
    };

    plugins = [
      {
        name = "nix_shell";
        src = pkgs.zsh-nix-shell;
        file = "share/zsh-nix-shell/nix-shell.plugin.zsh";
      }
      {
        name = "nix_completion";
        src = pkgs.nix-zsh-completions;
        file = "share/zsh/plugins/nix/nix-zsh-completions.plugin.zsh";
      }
      {
        name = "fast_syntax_highlight";
        src = pkgs.zsh-fast-syntax-highlighting;
        file = "share/zsh/site-functions/fast-syntax-highlighting.plugin.zsh";
      }
      {
        name = "pure_prompt";
        src = pkgs.fetchFromGitHub {
          owner = "sindresorhus";
          repo = "pure";
          rev =  "47c0c881f0e7cfdb5eaccd335f52ad17b897c060";
          sha256 =  "15xdhi72pq88ls5gx1h0k23jvb41j6kq6ar17dqmd5d38zsgwl3v";
        };
        file = "pure.plugin.zsh";
      }
      {
        name = "fzf";
        src = pkgs.fzf-zsh;
        file = "share/zsh/plugins/fzf-zsh/fzf-zsh.plugin.zsh";
      }
      {
        name = "zsh-sudo";
        src = pkgs.fetchFromGitHub {
          owner = "hcgraf";
          repo = "zsh-sudo";
          rev = "d8084def6bb1bde2482e7aa636743f40c69d9b32";
          sha256 = "1dpm51w3wjxil8sxqw4qxim5kmf6afmkwz1yfhldpdlqm7rfwpi3";
        };
        file = "sudo.plugin.zsh";
      }
    ];

    envExtra = '' '';

    initExtra = ''
      # binds
      bindkey '^K' fzf-file-widget

      # options
      setopt nobeep
      setopt nopromptcr
      setopt c_bases
      setopt completeinword
      setopt completealiases
      setopt extendedglob
      setopt notify

      # 
      RPS1=""

      #
      function chpwd() {
        ls;
      }

      eval "$(direnv hook zsh)"
      zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
    '';
  };
}
