{ config, lib, pkgs, ... }:
{

  programs.i3status-rust = {
    bars.top = {

      icons = "awesome5";
      theme = "gruvbox-dark";
      settings.theme = {
        theme = "plain";
        overrides = {
          separator_fg = "#3287a8";
        };
      };

      blocks = [
        {
          block = "battery";
          interval = 10;
          format = "$icon $percentage $time";
        }
        {
          block = "disk_space";
          path = "/";
          info_type = "available";
          interval = 20;
          warning = 20.0;
          alert = 10.0;
        }
        {
          block = "net";
          device = "wlan0";
          interval = 2;
        }
        {
          block = "net";
          device = "enp0s25";
          interval = 2;
        }
        {
          block = "memory";
        }
        {
          block = "cpu";
          interval = 1;
          format = "$utilization $barchart $frequency";
        }
        {
          block = "temperature";
          interval = 3;
        }
        {
          block = "sound";
        }
        {
          block = "time";
          interval = 60;
        }
      ];
    };
  };
}
