{ pkgs, vimUtils, fetchFromGitHub }:
{

  extraPackages = [
    pkgs.ccls
    pkgs.clang
    pkgs.luaformatter
    pkgs.nil
    pkgs.pyright
    pkgs.rnix-lsp
    pkgs.rust-analyzer
    pkgs.sumneko-lua-language-server
    pkgs.svls
    pkgs.texlab
    pkgs.tree-sitter
    pkgs.zls
    pkgs.verible
  ];

  plugins = with pkgs.vimPlugins; [
    colorizer
    fugitive
    fzf-vim
    gruvbox
    nvim-lspconfig
    nvim-treesitter
    nvim-ts-rainbow
    repeat
    targets-vim
    UltiSnips
    vim-addon-nix
    vim-signify
    vim-slime
    vim-snippets
    zig-vim
    cmp-nvim-lsp
    cmp-buffer
    cmp-path
    cmp-cmdline
    nvim-cmp
  ];
}
