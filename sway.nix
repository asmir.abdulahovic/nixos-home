{ config, lib, pkgs, ... }: {
  wayland.windowManager.sway = {
    enable = true;
    extraSessionCommands = "";

    config = {
      fonts = {
        names = [ "JetBrainsMono" ];
        style = "Bold Semi-Condensed";
        size = 11.0;
      };

      window.commands = [
        {
          command = "move scratchpad, resize set 1152 648";
          criteria = { app_id = "pulsemixer|python3|whatsapp-for-linux|com.viber.Viber"; };
        }
        {
          command = "move scratchpad, resize set 1502 845";
          criteria = { class = "ViberPC"; };
        }
        {
          command = "floating enable";
          criteria = { app_id = "sws_cli"; };
        }
      ];

      modifier = "Mod4";
      output = {
        eDP-1 = {
          bg = "~/pic/space_jellyfisy.jpg stretch";
          /*scale = "1.4";*/
        };
      };
      input = {
        "1:1:AT_Translated_Set_2_keyboard" = { repeat_delay = "150"; };
        "1:1:AT_Translated_Set_2_keyboard" = { repeat_rate = "70"; };
        "2:7:SynPS/2_Synaptics_TouchPad" = { tap = "enabled"; };
      };

      bars = [{
        position = "top";
        fonts = {
          names = [ "Iosevka" "FontAwesome" ];
          style = "Bold Semi-Condensed";
          size = 12.0;
        };
        statusCommand = "i3status-rs ~/.config/i3status-rust/config-top.toml";
      }];

      keybindings = {
        "Alt+Shift+Return" = "exec ${pkgs.foot}/bin/footclient";
        "Alt+p" = "exec ${pkgs.dmenu-wayland}/bin/dmenu-wl_run -fn \"mono 14\"";

        "Alt+Shift+space" = "floating toggle";
        "Alt+space" = "focus mode_toggle";

        "Alt+m" = "layout toggle splith tabbed";
        "Alt+t" = "split toggle";
        "Alt+s" = "layout toggle split";

        "Alt+1" = "workspace 1";
        "Alt+2" = "workspace 2";
        "Alt+3" = "workspace 3";
        "Alt+4" = "workspace 4";
        "Alt+5" = "workspace 5";
        "Alt+6" = "workspace 6";
        "Alt+7" = "workspace 7";
        "Alt+8" = "workspace 8";
        "Alt+9" = "workspace 9";
        "Alt+0" = "workspace 10";

        "Alt+Shift+1" = "move container to workspace 1";
        "Alt+Shift+2" = "move container to workspace 2";
        "Alt+Shift+3" = "move container to workspace 3";
        "Alt+Shift+4" = "move container to workspace 4";
        "Alt+Shift+5" = "move container to workspace 5";
        "Alt+Shift+6" = "move container to workspace 6";
        "Alt+Shift+7" = "move container to workspace 7";
        "Alt+Shift+8" = "move container to workspace 8";
        "Alt+Shift+9" = "move container to workspace 9";
        "Alt+Shift+0" = "move container to workspace 10";

        "Alt+h" = "focus left";
        "Alt+j" = "focus down";
        "Alt+k" = "focus up";
        "Alt+l" = "focus right";
        "Alt+slash" =
          "exec lckdo /tmp/.sws_cli_lock footclient -a sws_cli -- sws_cli.sh";
        "Alt+Escape" = "workspace back_and_forth";
        "Alt+f" = "fullscreen enable";

        "Mod4+l" = ''
          exec swaymsg [app_id="python3"] scratchpad show || exec foot -a python3 python3'';
        "Mod4+h" =
          "exec swaymsg [app_id=whatsapp-for-linux] scratchpad show || exec whatsapp-for-linux";
        "Mod4+j" =
          "exec swaymsg [app_id=com.viber.Viber] scratchpad show";
        "Mod4+y" = ''
          exec swaymsg [app_id="pulsemixer"] scratchpad show || exec foot -a pulsemixer pulsemixer'';

        "XF86AudioRaiseVolume" =
          "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') +5%";
        "XF86AudioLowerVolume" =
          "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') -5%";
        "XF86AudioMute" =
          "exec pactl set-sink-mute $(pacmd list-sinks |awk '/* index:/{print $3}') toggle";
        "XF86AudioMicMute" =
          "exec pactl set-source-mute $(pacmd list-sources |awk '/* index:/{print $3}') toggle";
      };
    };
  };
}
